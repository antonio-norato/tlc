<section class="home-slider owl-carousel">
    <div class="slider-item" style="background-image: url('images/Slider1.jpg');">
        <div class="overlay"></div>
        <div class="container">
            <div class="row slider-text align-items-center justify-content-center text-center">
                <div class="col-md-10 col-sm-12 ftco-animate">
                    <h1 class="mb-3">Great Food. Great Events. Anywhere. Anytime.
                    </h1>
                    <p><a href="reservation.html" class="btn btn-primary btn-outline-white px-5 py-3">Book the London Caterer</a></p>
                </div>
            </div>
        </div>
    </div>

    <div class="slider-item" style="background-image: url('images/Slider2.jpg');">
        <div class="overlay"></div>
        <div class="container">
            <div class="row slider-text align-items-center justify-content-center text-center">
                <div class="col-md-10 col-sm-12 ftco-animate">
                    <h1 class="mb-3">The London Caterer <br> Made with love</h1>
                    <p><a href="reservation.html" class="btn btn-primary btn-outline-white px-5 py-3">Book the London Caterer</a></p>
                </div>
            </div>
        </div>
    </div>

    <div class="slider-item" style="background-image: url('images/Slider3.jpg');">
        <div class="overlay"></div>
        <div class="container">
            <div class="row slider-text align-items-center justify-content-center text-center">
                <div class="col-md-10 col-sm-12 ftco-animate">
                    <h1 class="mb-3">The London Caterer <br> Food, people, craft
                    </h1>
                    <p><a href="reservation.html" class="btn btn-primary btn-outline-white px-5 py-3">Book the London Caterer</a></p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END slider -->
