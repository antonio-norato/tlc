<div class="row">
    <div class="col-md-12 dish-menu">

        <div class="nav nav-pills justify-content-center ftco-animate" id="v-pills-tab" role="tablist" aria-orientation="vertical">
            <a class="nav-link py-3 px-4 active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true"><span class="flaticon-dish"></span> Appetizers</a>
            <a class="nav-link py-3 px-4" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false"><span class="flaticon-cutlery"></span> Pastas</a>
            <a class="nav-link py-3 px-4" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false"><span class="flaticon-meat"></span> Second courses</a>
        </div>

        <div class="tab-content py-5" id="v-pills-tabContent">
            <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="menus d-flex ftco-animate">
                            <div class="menu-img" style="background-image: url(images/dish-3.jpg);"></div>
                            <div class="text d-flex">
                                <div class="one-half">
                                    <h3>Appetizer 1</h3>
                                    <p><span>Ingredient1</span>, <span>Ingredient2</span>, <span>Ingredient3</span>, <span>Ingredient4</span></p>
                                </div>
                                <div class="one-forth">
                                    <span class="price"></span>
                                </div>
                            </div>
                        </div>
                        <div class="menus d-flex ftco-animate">
                            <div class="menu-img" style="background-image: url(images/dish-4.jpg);"></div>
                            <div class="text d-flex">
                                <div class="one-half">
                                    <h3>Appetizer 2</h3>
                                    <p><span>Ingredient1</span>, <span>Ingredient2</span>, <span>Ingredient3</span>, <span>Ingredient4</span></p>
                                </div>
                                <div class="one-forth">
                                    <span class="price"></span>
                                </div>
                            </div>
                        </div>
                        <div class="menus d-flex ftco-animate">
                            <div class="menu-img" style="background-image: url(images/dish-5.jpg);"></div>
                            <div class="text d-flex">
                                <div class="one-half">
                                    <h3>Appetizer 3</h3>
                                    <p><span>Ingredient1</span>, <span>Ingredient2</span>, <span>Ingredient3</span>, <span>Ingredient4</span></p>
                                </div>
                                <div class="one-forth">
                                    <span class="price"></span>
                                </div>
                            </div>
                        </div>
                        <div class="menus d-flex ftco-animate">
                            <div class="menu-img" style="background-image: url(images/dish-6.jpg);"></div>
                            <div class="text d-flex">
                                <div class="one-half">
                                    <h3>Appetizer 4</h3>
                                    <p><span>Ingredient1</span>, <span>Ingredient2</span>, <span>Ingredient3</span>, <span>Ingredient4</span></p>
                                </div>
                                <div class="one-forth">
                                    <span class="price"></span>
                                </div>
                            </div>
                        </div>
                        <div class="menus d-flex ftco-animate">
                            <div class="menu-img" style="background-image: url(images/dish-7.jpg);"></div>
                            <div class="text d-flex">
                                <div class="one-half">
                                    <h3>Appetizer 5</h3>
                                    <p><span>Ingredient1</span>, <span>Ingredient2</span>, <span>Ingredient3</span>, <span>Ingredient4</span></p>
                                </div>
                                <div class="one-forth">
                                    <span class="price"></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="menus d-flex ftco-animate">
                            <div class="menu-img" style="background-image: url(images/dish-8.jpg);"></div>
                            <div class="text d-flex">
                                <div class="one-half">
                                    <h3>Appetizer 6</h3>
                                    <p><span>Ingredient1</span>, <span>Ingredient2</span>, <span>Ingredient3</span>, <span>Ingredient4</span></p>
                                </div>
                                <div class="one-forth">
                                    <span class="price"></span>
                                </div>
                            </div>
                        </div>
                        <div class="menus d-flex ftco-animate">
                            <div class="menu-img" style="background-image: url(images/dish-9.jpg);"></div>
                            <div class="text d-flex">
                                <div class="one-half">
                                    <h3>Appetizer 7</h3>
                                    <p><span>Ingredient1</span>, <span>Ingredient2</span>, <span>Ingredient3</span>, <span>Ingredient4</span></p>
                                </div>
                                <div class="one-forth">
                                    <span class="price"></span>
                                </div>
                            </div>
                        </div>
                        <div class="menus d-flex ftco-animate">
                            <div class="menu-img" style="background-image: url(images/dish-10.jpg);"></div>
                            <div class="text d-flex">
                                <div class="one-half">
                                    <h3>Appetizer 8</h3>
                                    <p><span>Ingredient1</span>, <span>Ingredient2</span>, <span>Ingredient3</span>, <span>Ingredient4</span></p>
                                </div>
                                <div class="one-forth">
                                    <span class="price"></span>
                                </div>
                            </div>
                        </div>
                        <div class="menus d-flex ftco-animate">
                            <div class="menu-img" style="background-image: url(images/dish-11.jpg);"></div>
                            <div class="text d-flex">
                                <div class="one-half">
                                    <h3>Appetizer 9</h3>
                                    <p><span>Ingredient1</span>, <span>Ingredient2</span>, <span>Ingredient3</span>, <span>Ingredient4</span></p>
                                </div>
                                <div class="one-forth">
                                    <span class="price"></span>
                                </div>
                            </div>
                        </div>
                        <div class="menus d-flex ftco-animate">
                            <div class="menu-img" style="background-image: url(images/dish-12.jpg);"></div>
                            <div class="text d-flex">
                                <div class="one-half">
                                    <h3>Appetizer 10</h3>
                                    <p><span>Ingredient1</span>, <span>Ingredient2</span>, <span>Ingredient3</span>, <span>Ingredient4</span></p>
                                </div>
                                <div class="one-forth">
                                    <span class="price"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- END -->

            <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="menus d-flex ftco-animate">
                            <div class="menu-img" style="background-image: url(images/dessert-1.jpg);"></div>
                            <div class="text d-flex">
                                <div class="one-half">
                                    <h3>Pasta 1</h3>
                                    <p><span>Ingredient1</span>, <span>Ingredient2</span>, <span>Ingredient3</span>, <span>Ingredient4</span></p>
                                </div>
                                <div class="one-forth">
                                    <span class="price"></span>
                                </div>
                            </div>
                        </div>
                        <div class="menus d-flex ftco-animate">
                            <div class="menu-img" style="background-image: url(images/dessert-2.jpg);"></div>
                            <div class="text d-flex">
                                <div class="one-half">
                                    <h3>Pasta 2</h3>
                                    <p><span>Ingredient1</span>, <span>Ingredient2</span>, <span>Ingredient3</span>, <span>Ingredient4</span></p>
                                </div>
                                <div class="one-forth">
                                    <span class="price"></span>
                                </div>
                            </div>
                        </div>
                        <div class="menus d-flex ftco-animate">
                            <div class="menu-img" style="background-image: url(images/dessert-3.jpg);"></div>
                            <div class="text d-flex">
                                <div class="one-half">
                                    <h3>Pasta 3</h3>
                                    <p><span>Ingredient1</span>, <span>Ingredient2</span>, <span>Ingredient3</span>, <span>Ingredient4</span></p>
                                </div>
                                <div class="one-forth">
                                    <span class="price"></span>
                                </div>
                            </div>
                        </div>
                        <div class="menus d-flex ftco-animate">
                            <div class="menu-img" style="background-image: url(images/dessert-4.jpg);"></div>
                            <div class="text d-flex">
                                <div class="one-half">
                                    <h3>Pasta 4</h3>
                                    <p><span>Ingredient1</span>, <span>Ingredient2</span>, <span>Ingredient3</span>, <span>Ingredient4</span></p>
                                </div>
                                <div class="one-forth">
                                    <span class="price"></span>
                                </div>
                            </div>
                        </div>
                        <div class="menus d-flex ftco-animate">
                            <div class="menu-img" style="background-image: url(images/dessert-5.jpg);"></div>
                            <div class="text d-flex">
                                <div class="one-half">
                                    <h3>Pasta 5</h3>
                                    <p><span>Ingredient1</span>, <span>Ingredient2</span>, <span>Ingredient3</span>, <span>Ingredient4</span></p>
                                </div>
                                <div class="one-forth">
                                    <span class="price"></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="menus d-flex ftco-animate">
                            <div class="menu-img" style="background-image: url(images/dessert-6.jpg);"></div>
                            <div class="text d-flex">
                                <div class="one-half">
                                    <h3>Pasta 6</h3>
                                    <p><span>Ingredient1</span>, <span>Ingredient2</span>, <span>Ingredient3</span>, <span>Ingredient4</span></p>
                                </div>
                                <div class="one-forth">
                                    <span class="price"></span>
                                </div>
                            </div>
                        </div>
                        <div class="menus d-flex ftco-animate">
                            <div class="menu-img" style="background-image: url(images/dessert-7.jpg);"></div>
                            <div class="text d-flex">
                                <div class="one-half">
                                    <h3>Pasta 7</h3>
                                    <p><span>Ingredient1</span>, <span>Ingredient2</span>, <span>Ingredient3</span>, <span>Ingredient4</span></p>
                                </div>
                                <div class="one-forth">
                                    <span class="price"></span>
                                </div>
                            </div>
                        </div>
                        <div class="menus d-flex ftco-animate">
                            <div class="menu-img" style="background-image: url(images/dessert-8.jpg);"></div>
                            <div class="text d-flex">
                                <div class="one-half">
                                    <h3>Pasta 8</h3>
                                    <p><span>Ingredient1</span>, <span>Ingredient2</span>, <span>Ingredient3</span>, <span>Ingredient4</span></p>
                                </div>
                                <div class="one-forth">
                                    <span class="price"></span>
                                </div>
                            </div>
                        </div>
                        <div class="menus d-flex ftco-animate">
                            <div class="menu-img" style="background-image: url(images/dessert-9.jpg);"></div>
                            <div class="text d-flex">
                                <div class="one-half">
                                    <h3>Pasta 9</h3>
                                    <p><span>Ingredient1</span>, <span>Ingredient2</span>, <span>Ingredient3</span>, <span>Ingredient4</span></p>
                                </div>
                                <div class="one-forth">
                                    <span class="price"></span>
                                </div>
                            </div>
                        </div>
                        <div class="menus d-flex ftco-animate">
                            <div class="menu-img" style="background-image: url(images/dessert-10.jpg);"></div>
                            <div class="text d-flex">
                                <div class="one-half">
                                    <h3>Pasta 10</h3>
                                    <p><span>Ingredient1</span>, <span>Ingredient2</span>, <span>Ingredient3</span>, <span>Ingredient4</span></p>
                                </div>
                                <div class="one-forth">
                                    <span class="price"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- END -->

            <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="menus d-flex ftco-animate">
                            <div class="menu-img" style="background-image: url(images/drink-1.jpg);"></div>
                            <div class="text d-flex">
                                <div class="one-half">
                                    <h3>Second course 1</h3>
                                    <p><span>Ingredient1</span>, <span>Ingredient2</span>, <span>Ingredient3</span>, <span>Ingredient4</span></p>
                                </div>
                                <div class="one-forth">
                                    <span class="price"></span>
                                </div>
                            </div>
                        </div>
                        <div class="menus d-flex ftco-animate">
                            <div class="menu-img" style="background-image: url(images/drink-2.jpg);"></div>
                            <div class="text d-flex">
                                <div class="one-half">
                                    <h3>Second course 2</h3>
                                    <p><span>Ingredient1</span>, <span>Ingredient2</span>, <span>Ingredient3</span>, <span>Ingredient4</span></p>
                                </div>
                                <div class="one-forth">
                                    <span class="price"></span>
                                </div>
                            </div>
                        </div>
                        <div class="menus d-flex ftco-animate">
                            <div class="menu-img" style="background-image: url(images/drink-3.jpg);"></div>
                            <div class="text d-flex">
                                <div class="one-half">
                                    <h3>Second course 3</h3>
                                    <p><span>Ingredient1</span>, <span>Ingredient2</span>, <span>Ingredient3</span>, <span>Ingredient4</span></p>
                                </div>
                                <div class="one-forth">
                                    <span class="price"></span>
                                </div>
                            </div>
                        </div>
                        <div class="menus d-flex ftco-animate">
                            <div class="menu-img" style="background-image: url(images/drink-4.jpg);"></div>
                            <div class="text d-flex">
                                <div class="one-half">
                                    <h3>Second course 4</h3>
                                    <p><span>Ingredient1</span>, <span>Ingredient2</span>, <span>Ingredient3</span>, <span>Ingredient4</span></p>
                                </div>
                                <div class="one-forth">
                                    <span class="price"></span>
                                </div>
                            </div>
                        </div>
                        <div class="menus d-flex ftco-animate">
                            <div class="menu-img" style="background-image: url(images/drink-5.jpg);"></div>
                            <div class="text d-flex">
                                <div class="one-half">
                                    <h3>Second course 5</h3>
                                    <p><span>Ingredient1</span>, <span>Ingredient2</span>, <span>Ingredient3</span>, <span>Ingredient4</span></p>
                                </div>
                                <div class="one-forth">
                                    <span class="price"></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="menus d-flex ftco-animate">
                            <div class="menu-img" style="background-image: url(images/drink-6.jpg);"></div>
                            <div class="text d-flex">
                                <div class="one-half">
                                    <h3>Second course 6</h3>
                                    <p><span>Ingredient1</span>, <span>Ingredient2</span>, <span>Ingredient3</span>, <span>Ingredient4</span></p>
                                </div>
                                <div class="one-forth">
                                    <span class="price"></span>
                                </div>
                            </div>
                        </div>
                        <div class="menus d-flex ftco-animate">
                            <div class="menu-img" style="background-image: url(images/drink-7.jpg);"></div>
                            <div class="text d-flex">
                                <div class="one-half">
                                    <h3>Second course 7</h3>
                                    <p><span>Ingredient1</span>, <span>Ingredient2</span>, <span>Ingredient3</span>, <span>Ingredient4</span></p>
                                </div>
                                <div class="one-forth">
                                    <span class="price"></span>
                                </div>
                            </div>
                        </div>
                        <div class="menus d-flex ftco-animate">
                            <div class="menu-img" style="background-image: url(images/drink-8.jpg);"></div>
                            <div class="text d-flex">
                                <div class="one-half">
                                    <h3>Second course 8</h3>
                                    <p><span>Ingredient1</span>, <span>Ingredient2</span>, <span>Ingredient3</span>, <span>Ingredient4</span></p>
                                </div>
                                <div class="one-forth">
                                    <span class="price"></span>
                                </div>
                            </div>
                        </div>
                        <div class="menus d-flex ftco-animate">
                            <div class="menu-img" style="background-image: url(images/drink-9.jpg);"></div>
                            <div class="text d-flex">
                                <div class="one-half">
                                    <h3>Second course 9</h3>
                                    <p><span>Ingredient1</span>, <span>Ingredient2</span>, <span>Ingredient3</span>, <span>Ingredient4</span></p>
                                </div>
                                <div class="one-forth">
                                    <span class="price"></span>
                                </div>
                            </div>
                        </div>
                        <div class="menus d-flex ftco-animate">
                            <div class="menu-img" style="background-image: url(images/drink-10.jpg);"></div>
                            <div class="text d-flex">
                                <div class="one-half">
                                    <h3>Second course 10</h3>
                                    <p><span>Ingredient1</span>, <span>Ingredient2</span>, <span>Ingredient3</span>, <span>Ingredient4</span></p>
                                </div>
                                <div class="one-forth">
                                    <span class="price"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 pt-4 text-center ftco-animate">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
                <span><a href="templateHtml/reservation.html" class="btn btn-primary btn-outline-primary p-3">Book the London Caterer</a></span>
            </div>
        </div>
    </div>
</div>