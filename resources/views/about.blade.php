@include( 'layouts.master' )
@include( 'layouts.navigation' )
<section class="home-slider owl-carousel">
    <div class="slider-item" style="background-image: url('images/Slider3.jpg');" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row slider-text align-items-center justify-content-center">
                <div class="col-md-10 col-sm-12 ftco-animate text-center">
                    <p class="breadcrumbs"><span class="mr-2"><a href="/">Home</a></span> <span>About</span></p>
                    <h1 class="mb-3">About Us</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section-2">
    <div class="container d-flex">
        <div class="section-2-blocks-wrapper row">
            <div class="img col-sm-12 col-lg-6" style="background-image: url('images/about-2.jpg');">
            </div>
            <div class="text col-lg-6 ftco-animate">
                <div class="text-inner align-self-start">
                    <span class="subheading">About The London Caterer</span>
                    <h3 class="heading">Our chef cooks the most delicious food for you</h3>
                    <p>The London Caterer is a fresh and youthful food catering company, focusing on bespoke packages for your private dinners, private events or simply a private chef at your doorstep; tailored to the needs of every single client with the highest level of discretion, professionalism, flexibility and service.</p>
                        <p>The London Caterer was born from the experience of a highly rewarded and recognised chef on the Central London restaurant scene, alongside a credited and successful event co-ordinator. With over 15 years combined experience, The London Caterer ensures your event is seamless: with executive planning, delicious food and flawless service with a personal touch.</p>
                        <p>Working flexibly together with clients, The London Caterer can tailor a menu to suit your every need and offers conscious choices for a wide range of dietary requirements with every aspect of your event catering taken care of.</p>
                        Visit Our Services or Get in Touch
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="ftco-section">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-5">
            <div class="col-md-7 text-center heading-section ftco-animate">
                <span class="subheading">Our Chef</span>
                <h2>Our Master Chef</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 ftco-animate">
                <div class="block-10">
                    <div class="person-info">
                        <span class="name">Thomas Smith</span>
                        <span class="position">Head Chef</span>
                    </div>
                    <div class="chef-img" style="background-image: url(images/chef-1.jpg)"></div>
                </div>
            </div>
            <div class="col-md-4 ftco-animate">
                <div class="block-10">
                    <div class="person-info">
                        <span class="name">Francis Gibson</span>
                        <span class="position">Assistant Chef</span>
                    </div>
                    <div class="chef-img" style="background-image: url(images/chef-2.jpg)"></div>
                </div>
            </div>
            <div class="col-md-4 ftco-animate">
                <div class="block-10">
                    <div class="person-info">
                        <span class="name">Angelo Maestro</span>
                        <span class="position">Assistant Chef</span>
                    </div>
                    <div class="chef-img" style="background-image: url(images/chef-3.jpg)"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@include( 'layouts.instagram' )
@include( 'layouts.footer' )
@include( 'layouts.javascript' )
</body>
</html>
