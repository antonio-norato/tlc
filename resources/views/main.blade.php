@include( 'layouts.master' )
@include( 'layouts.navigation' )
@include( 'layouts.carousel' )
@include( 'layouts.reservation' )

<section class="ftco-section-2">
    <div class="container d-flex">
        <div class="section-2-blocks-wrapper row">
            <div class="img col-sm-12 col-lg-6" style="background-image: url('images/foto2.jpg');">
            </div>
            <div class="text col-lg-6 ftco-animate">
                <div class="text-inner align-self-start">
                    <span class="subheading"></span>
                    <h3 class="heading">About The London Caterer</h3>
                    <p>The London Caterer offers a wide range of services for private dinners for up to 20 guests, and event catering and corporate functions for larger parties; whether it is a seated dinner, canape reception, bowl food or buffet. </p><p>Planned and created to our client’s briefs, a personalised menu is created for each and every event.</p>

                </div>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section bg-light">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-5">
            <div class="col-md-7 text-center heading-section ftco-animate">
                <span class="subheading">Our dishes</span>
                <h2>Discover Our Exclusive dishes</h2>
            </div>
        </div>
        @include( 'layouts.dishes' );
    </div>
</section>

<section class="ftco-section parallax-img" style="background-image: url('images/Background.jpg');" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row justify-content-center mb-5 pb-5">
            <div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
                <h2>Our Specialties</h2>
            </div>
        </div>
    </div>
</section>
<section class="ftco-section bg-light">
    <div class="container special-dish">
        <div class="row d-flex no-gutters">
            <div class="col-lg-6">
                <div class="block-3 d-md-flex ftco-animate">
                    <div class="image order-last" style="background-image: url(images/dish1.jpg);"></div>
                    <div class="text text-center order-first">
                        <h2 class="heading">Dish 1</h2>
                        <p>Accusamus aperiam beatae blanditiis cupiditate dolor dolores doloribus enim eos illo inventore minus mollitia nesciunt perspiciatis, reiciendis reprehenderit sequi soluta totam voluptates</p>
                        <span class="price"></span>
                    </div>
                </div>
                <div class="block-3 d-md-flex ftco-animate">
                    <div class="image order-first" style="background-image: url(images/dish2.jpg);"></div>
                    <div class="text text-center order-first">
                        <h2 class="heading">Dish 2</h2>
                        <p>Accusamus aperiam beatae blanditiis cupiditate dolor dolores doloribus enim eos illo inventore minus mollitia nesciunt perspiciatis, reiciendis reprehenderit sequi soluta totam voluptates</p>
                        <span class="price"></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="block-3 d-md-flex ftco-animate">
                    <div class="image order-last" style="background-image: url(images/dish3.jpg);"></div>
                    <div class="text text-center order-first">
                        <h2 class="heading">Dish 3</h2>
                        <p>Accusamus aperiam beatae blanditiis cupiditate dolor dolores doloribus enim eos illo inventore minus mollitia nesciunt perspiciatis, reiciendis reprehenderit sequi soluta totam voluptates</p>
                        <span class="price"></span>
                    </div>
                </div>
                <div class="block-3 d-md-flex ftco-animate">
                    <div class="image order-first" style="background-image: url(images/dish4.jpg);"></div>
                    <div class="text text-center order-first">
                        <h2 class="heading">Dish 4</h2>
                        <p>Accusamus aperiam beatae blanditiis cupiditate dolor dolores doloribus enim eos illo inventore minus mollitia nesciunt perspiciatis, reiciendis reprehenderit sequi soluta totam voluptates</p>
                        <span class="price"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section testimony-section">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-5">
            <div class="col-md-7 text-center heading-section ftco-animate">
                <span class="subheading">Clients Say</span>
                <h2>Our Satisfied Clients say</h2>
            </div>
        </div>
        <div class="row ftco-animate">
            <div class="carousel owl-carousel ftco-owl">
                <div class="item text-center">
                    <div class="testimony-wrap p-4 pb-5">
                        <div class="user-img mb-4" style="background-image: url(images/person_1.jpg)" style="border: 1px solid red;"></div>
                        <div class="text">
                            <p class="star-rate"><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star-half-full"></span></p>
                            <p class="mb-5">Accusamus aperiam beatae blanditiis cupiditate dolor dolores doloribus enim eos illo inventore minus mollitia nesciunt perspiciatis, reiciendis reprehenderit sequi soluta totam voluptates.</p>
                            <p class="name">Dennis Green</p>
                            <span class="position">Guests from Italy</span>
                        </div>
                    </div>
                </div>
                <div class="item text-center">
                    <div class="testimony-wrap p-4 pb-5">
                        <div class="user-img mb-4" style="background-image: url(images/person_2.jpg)" style="border: 1px solid red;"></div>
                        <div class="text">
                            <p class="star-rate"><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star-half-full"></span></p>
                            <p class="mb-5">Accusamus aperiam beatae blanditiis cupiditate dolor dolores doloribus enim eos illo inventore minus mollitia nesciunt perspiciatis, reiciendis reprehenderit sequi soluta totam voluptates.</p>
                            <p class="name">Dennis Green</p>
                            <span class="position">Guests from Italy</span>
                        </div>
                    </div>
                </div>
                <div class="item text-center">
                    <div class="testimony-wrap p-4 pb-5">
                        <div class="user-img mb-4" style="background-image: url(images/person_3.jpg)" style="border: 1px solid red;"></div>
                        <div class="text">
                            <p class="star-rate"><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star-half-full"></span></p>
                            <p class="mb-5">Accusamus aperiam beatae blanditiis cupiditate dolor dolores doloribus enim eos illo inventore minus mollitia nesciunt perspiciatis, reiciendis reprehenderit sequi soluta totam voluptates.</p>
                            <p class="name">Dennis Green</p>
                            <span class="position">Guests from Italy</span>
                        </div>
                    </div>
                </div>
                <div class="item text-center">
                    <div class="testimony-wrap p-4 pb-5">
                        <div class="user-img mb-4" style="background-image: url(images/person_1.jpg)" style="border: 1px solid red;"></div>
                        <div class="text">
                            <p class="star-rate"><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star-half-full"></span></p>
                            <p class="mb-5">Accusamus aperiam beatae blanditiis cupiditate dolor dolores doloribus enim eos illo inventore minus mollitia nesciunt perspiciatis, reiciendis reprehenderit sequi soluta totam voluptates.</p>
                            <p class="name">Dennis Green</p>
                            <span class="position">Guests from Italy</span>
                        </div>
                    </div>
                </div>
                <div class="item text-center">
                    <div class="testimony-wrap p-4 pb-5">
                        <div class="user-img mb-4" style="background-image: url(images/person_1.jpg)" style="border: 1px solid red;"></div>
                        <div class="text">
                            <p class="star-rate"><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star-half-full"></span></p>
                            <p class="mb-5">Accusamus aperiam beatae blanditiis cupiditate dolor dolores doloribus enim eos illo inventore minus mollitia nesciunt perspiciatis, reiciendis reprehenderit sequi soluta totam voluptates.</p>
                            <p class="name">Dennis Green</p>
                            <span class="position">Guests from Italy</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{--<section class="ftco-section bg-light">--}}
    {{--<div class="container">--}}
        {{--<div class="row justify-content-center mb-5 pb-5">--}}
            {{--<div class="col-md-7 text-center heading-section ftco-animate">--}}
                {{--<span class="subheading">Blog</span>--}}
                {{--<h2>Recent Blog</h2>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="row ftco-animate">--}}
            {{--<div class="carousel1 owl-carousel ftco-owl">--}}
                {{--<div class="item">--}}
                    {{--<div class="blog-entry">--}}
                        {{--<a href="templateHtml/blog-single.html" class="block-20" style="background-image: url('images/image_5.jpg');">--}}
                        {{--</a>--}}
                        {{--<div class="text p-4">--}}
                            {{--<div class="meta">--}}
                                {{--<div><a href="#">July 7, 2018</a></div>--}}
                                {{--<div><a href="#">Admin</a></div>--}}
                            {{--</div>--}}
                            {{--<h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>--}}
                            {{--<p class="clearfix">--}}
                                {{--<a href="#" class="float-left read">Read more</a>--}}
                                {{--<a href="#" class="float-right meta-chat"><span class="icon-chat"></span> 3</a>--}}
                            {{--</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="item">--}}
                    {{--<div class="blog-entry" data-aos-delay="100">--}}
                        {{--<a href="templateHtml/blog-single.html" class="block-20" style="background-image: url('images/image_6.jpg');">--}}
                        {{--</a>--}}
                        {{--<div class="text p-4">--}}
                            {{--<div class="meta">--}}
                                {{--<div><a href="#">July 7, 2018</a></div>--}}
                                {{--<div><a href="#">Admin</a></div>--}}
                            {{--</div>--}}
                            {{--<h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>--}}
                            {{--<p class="clearfix">--}}
                                {{--<a href="#" class="float-left read">Read more</a>--}}
                                {{--<a href="#" class="float-right meta-chat"><span class="icon-chat"></span> 3</a>--}}
                            {{--</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="item">--}}
                    {{--<div class="blog-entry" data-aos-delay="200">--}}
                        {{--<a href="templateHtml/blog-single.html" class="block-20" style="background-image: url('images/image_7.jpg');">--}}
                        {{--</a>--}}
                        {{--<div class="text p-4">--}}
                            {{--<div class="meta">--}}
                                {{--<div><a href="#">July 7, 2018</a></div>--}}
                                {{--<div><a href="#">Admin</a></div>--}}
                            {{--</div>--}}
                            {{--<h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>--}}
                            {{--<p class="clearfix">--}}
                                {{--<a href="#" class="float-left read">Read more</a>--}}
                                {{--<a href="#" class="float-right meta-chat"><span class="icon-chat"></span> 3</a>--}}
                            {{--</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="item">--}}
                    {{--<div class="blog-entry" data-aos-delay="200">--}}
                        {{--<a href="templateHtml/blog-single.html" class="block-20" style="background-image: url('images/image_8.jpg');">--}}
                        {{--</a>--}}
                        {{--<div class="text p-4">--}}
                            {{--<div class="meta">--}}
                                {{--<div><a href="#">July 7, 2018</a></div>--}}
                                {{--<div><a href="#">Admin</a></div>--}}
                            {{--</div>--}}
                            {{--<h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>--}}
                            {{--<p class="clearfix">--}}
                                {{--<a href="#" class="float-left read">Read more</a>--}}
                                {{--<a href="#" class="float-right meta-chat"><span class="icon-chat"></span> 3</a>--}}
                            {{--</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="item">--}}
                    {{--<div class="blog-entry" data-aos-delay="200">--}}
                        {{--<a href="templateHtml/blog-single.html" class="block-20" style="background-image: url('images/image_9.jpg');">--}}
                        {{--</a>--}}
                        {{--<div class="text p-4">--}}
                            {{--<div class="meta">--}}
                                {{--<div><a href="#">July 7, 2018</a></div>--}}
                                {{--<div><a href="#">Admin</a></div>--}}
                            {{--</div>--}}
                            {{--<h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>--}}
                            {{--<p class="clearfix">--}}
                                {{--<a href="#" class="float-left read">Read more</a>--}}
                                {{--<a href="#" class="float-right meta-chat"><span class="icon-chat"></span> 3</a>--}}
                            {{--</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</section>--}}
@include( 'layouts.instagram' )
@include( 'layouts.footer' )
@include( 'layouts.javascript' )
</body>
</html>
