@include( 'layouts.master' )
@include( 'layouts.navigation' )

<section class="home-slider owl-carousel">
    <div class="slider-item" style="background-image: url('images/Slider1.jpg');" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row slider-text align-items-center justify-content-center">
                <div class="col-md-10 col-sm-12 ftco-animate text-center">
                    <p class="breadcrumbs"><span class="mr-2"><a href="/">Home</a></span> <span>Reservation</span></p>
                    <h1 class="mb-3">Make a Reservation</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section">
    <div class="container">
        <div class="row no-gutters justify-content-center mb-5 pb-5">
            <div class="col-md-7 text-center heading-section ftco-animate">
                <h2>Make a Reservation</h2>
            </div>
        </div>
        <div class="row d-flex">
            <div class="col-md-4 ftco-animate img" style="background-image: url(images/foto2.jpg);"></div>
            <div class="col-md-8 ftco-animate makereservation p-5 bg-light">
                <form action="#">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Name</label>
                                <input type="text" class="form-control" placeholder="Your Name">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="text" class="form-control" placeholder="Your Email">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Phone#1</label>
                                <input type="text" class="form-control" placeholder="Phone#1">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Phone#2</label>
                                <input type="text" class="form-control" id="book_date" placeholder="Phone#2">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Time</label>
                                <input type="text" class="form-control" id="book_time" placeholder="Time">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Persons</label>
                                <div class="select-wrap one-third">
                                    <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                                    <select name="" id="" class="form-control">
                                        <option value="">less than 8</option>
                                        <option value="">between 8 and 15</option>
                                        <option value="">between 16 and 25</option>
                                        <option value="">more than 25</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 mt-3">
                            <div class="form-group">
                                <input type="submit" value="Make a Reservation" class="btn btn-primary py-3 px-5">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@include( 'layouts.instagram' )
@include( 'layouts.footer' )
@include( 'layouts.javascript' )
</body>
</html>
