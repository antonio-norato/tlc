@include( 'layouts.master' )
@include( 'layouts.navigation' )

<section class="home-slider owl-carousel">
    <div class="slider-item" style="background-image: url('images/Slider3.jpg');" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row slider-text align-items-center justify-content-center">
                <div class="col-md-10 col-sm-12 ftco-animate text-center">
                    <p class="breadcrumbs"><span class="mr-2"><a href="/">Home</a></span> <span>Menu</span></p>
                    <h1 class="mb-3">Discover Our Exclusive Menu</h1>
                </div>
            </div>
        </div>
    </div>
</section>

@include( 'layouts.reservation' )

<section class="ftco-section bg-light">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-5">
            <div class="col-md-7 text-center heading-section ftco-animate">
                <span class="subheading">Our Menu</span>
                <h2>Discover Our Exclusive Menu</h2>
            </div>
        </div>
        @include( 'layouts.dishes' );
    </div>
</section>

@include( 'layouts.instagram' )
@include( 'layouts.footer' )
@include( 'layouts.javascript' )

</body>
</html>
