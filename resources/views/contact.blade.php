@include( 'layouts.master' )
@include( 'layouts.navigation' )
<section class="home-slider owl-carousel">
    <div class="slider-item" style="background-image: url('images/Slider2.jpg');" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row slider-text align-items-center justify-content-center">
                <div class="col-md-10 col-sm-12 ftco-animate text-center">
                    <p class="breadcrumbs"><span class="mr-2"><a href="/">Home</a></span> <span>Contact</span></p>
                    <h1 class="mb-3">Contact Us</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section contact-section">
    <div class="container">
        <div class="row block-9 mb-4">
            <div class="col-md-6 pr-md-5 flex-column">
                <div class="row d-block flex-row">
                    <h2 class="h4 mb-4">Contact Information</h2>
                    <div class="col mb-3 d-flex py-4 border" style="background: white;">
                        <div class="align-self-center">
                            <p class="mb-0"><span>Address:</span> 7 Vanbrugh Cl, London E16 3TG</p>
                        </div>
                    </div>
                    <div class="col mb-3 d-flex py-4 border" style="background: white;">
                        <div class="align-self-center">
                            <p class="mb-0"><span>Phone:</span> <a href="tel://00447721382097">+44 7721 382097</a></p>
                        </div>
                    </div>
                    <div class="col mb-3 d-flex py-4 border" style="background: white;">
                        <div class="align-self-center">
                            <p class="mb-0"><span>Email:</span> <a href="mailto:theldncaterer@gmail.com">theldncaterer@gmail.com</a></p>
                        </div>
                    </div>
                    <div class="col mb-3 d-flex py-4 border" style="background: white;">
                        <div class="align-self-center">
                            <p class="mb-0"><span>Facebook</span> <a href="#">Facebook URL</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <form action="#">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Your Name">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Your Email">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Subject">
                    </div>
                    <div class="form-group">
                        <textarea name="" id="" cols="30" rows="7" class="form-control" placeholder="Message"></textarea>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Send Message" class="btn btn-primary py-3 px-5">
                    </div>
                </form>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-12" id="map"></div>
        </div>
    </div>
</section>
@include( 'layouts.instagram' )
@include( 'layouts.footer' )
@include( 'layouts.javascript' )
<script>
    function initMap() {
        var center = {lat: 51.515934, lng: 0.040270};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 13,
            center: center
        });
        var marker = new google.maps.Marker({
            position: center,
            map: map
        });
    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDvzMslqDxWzuPc2UgQkne2MRaiP0bjEiw&callback=initMap">
</script>
</body>
</html>
