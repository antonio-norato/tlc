@include( 'layouts.master' )
@include( 'layouts.navigation' )
<section class="home-slider owl-carousel">
    <div class="slider-item" style="background-image: url('images/Slider2.jpg');" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row slider-text align-items-center justify-content-center">
                <div class="col-md-10 col-sm-12 ftco-animate text-center">
                    <p class="breadcrumbs"><span class="mr-2"><a href="/">Home</a></span> <span>Specialties</span></p>
                    <h1 class="mb-3">Our Specialties</h1>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="ftco-section bg-light">
    <div class="container">
        <div class="row no-gutters justify-content-center mb-5 pb-5">
            <div class="col-md-7 text-center heading-section ftco-animate">
                <h2>Our Specialties</h2>
            </div>
        </div>
        <div style="margin-bottom: 25px;">
            <p>The London Caterer focusses on high quality ingredients and recipes from across the Mediterranean; drawing inspiration from regions of Italy. Dishes are created using balanced flavours combined with contemporary flare, to produce a memorable meal for your special occasion.
            </p>
        </div>

        <div class="row d-flex no-gutters">
            <div class="col-lg-6">
                <div class="block-3 d-md-flex ftco-animate">
                    <div class="image order-last" style="background-image: url(images/dish1.jpg);"></div>
                    <div class="text text-center order-first">
                        <h2 class="heading">Specialty 1</h2>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts</p>
                        <span class="price"></span>
                    </div>
                </div>
                <div class="block-3 d-md-flex ftco-animate">
                    <div class="image order-first" style="background-image: url(images/dish2.jpg);"></div>
                    <div class="text text-center order-first">
                        <h2 class="heading">Specialty 2</h2>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts</p>
                        <span class="price"></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="block-3 d-md-flex ftco-animate">
                    <div class="image order-last" style="background-image: url(images/dish3.jpg);"></div>
                    <div class="text text-center order-first">
                        <h2 class="heading">Specialty 3</h2>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts</p>
                        <span class="price"></span>
                    </div>
                </div>
                <div class="block-3 d-md-flex ftco-animate">
                    <div class="image order-first" style="background-image: url(images/dish4.jpg);"></div>
                    <div class="text text-center order-first">
                        <h2 class="heading">Specialty 4</h2>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts</p>
                        <span class="price"></span>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="block-3 d-md-flex ftco-animate">
                    <div class="image order-last" style="background-image: url(images/dish5.jpg);"></div>
                    <div class="text text-center order-first">
                        <h2 class="heading">Specialty 5</h2>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts</p>
                        <span class="price"></span>
                    </div>
                </div>
                <div class="block-3 d-md-flex ftco-animate">
                    <div class="image order-first" style="background-image: url(images/dish6.jpg);"></div>
                    <div class="text text-center order-first">
                        <h2 class="heading">Specialty 6</h2>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts</p>
                        <span class="price"></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="block-3 d-md-flex ftco-animate">
                    <div class="image order-last" style="background-image: url(images/dish7.jpg);"></div>
                    <div class="text text-center order-first">
                        <h2 class="heading">Specialty 7</h2>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts</p>
                        <span class="price"></span>
                    </div>
                </div>
                <div class="block-3 d-md-flex ftco-animate">
                    <div class="image order-first" style="background-image: url(images/dish8.jpg);"></div>
                    <div class="text text-center order-first">
                        <h2 class="heading">Specialty 8</h2>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts</p>
                        <span class="price"></span>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="block-3 d-md-flex ftco-animate">
                    <div class="image order-last" style="background-image: url(images/dish1.jpg);"></div>
                    <div class="text text-center order-first">
                        <h2 class="heading">Specialty 9</h2>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts</p>
                        <span class="price"></span>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="block-3 d-md-flex ftco-animate">
                    <div class="image order-last" style="background-image: url(images/dish3.jpg);"></div>
                    <div class="text text-center order-first">
                        <h2 class="heading">Specialty 10</h2>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts</p>
                        <span class="price"></span>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
@include( 'layouts.instagram' )
@include( 'layouts.footer' )
@include( 'layouts.javascript' )
</body>
</html>
